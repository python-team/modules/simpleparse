simpleparse (2.2.0-2) UNRELEASED; urgency=medium

  * d/control: Set Vcs-* to salsa.debian.org
  * d/watch: Use https protocol
  * d/changelog: Remove trailing whitespaces
  * Convert git repository from git-dpm to gbp layout
  * d/control: Update Maintainer field with new Debian Python Team
    contact address.
  * d/control: Update Vcs-* fields with new Debian Python Team Salsa
    layout.

 -- Ondřej Nový <onovy@debian.org>  Tue, 13 Feb 2018 10:00:02 +0100

simpleparse (2.2.0-1) unstable; urgency=medium

  [ Ondřej Nový ]
  * Fixed VCS URL (https)

  [ Vincent Bernat ]
  * New upstream release.
    - drop "with"-keyword patch
    - fix FTBFS (Closes: #824738)
  * Switch to pybuild.
  * Bump Standards-Version.
  * Remove SF logo from documentation.

 -- Vincent Bernat <bernat@debian.org>  Sun, 25 Dec 2016 10:39:11 +0100

simpleparse (2.1.0a1-7) unstable; urgency=low

  [ Jakub Wilk ]
  * Use canonical URIs for Vcs-* fields.

  [ Vincent Bernat ]
  * Move to "3.0 (quilt)" format.
  * Move to dh-python.
  * Bump Standards-Version.
  * Update debian/watch to use pypi redirector.

 -- Vincent Bernat <bernat@debian.org>  Tue, 18 Aug 2015 14:07:18 +0200

simpleparse (2.1.0a1-6) unstable; urgency=low

  * Team upload.
  * No-change rebuild against python-defaults (>= 2.6.6-14).

 -- Jakub Wilk <jwilk@debian.org>  Sun, 01 May 2011 16:50:24 +0200

simpleparse (2.1.0a1-5) unstable; urgency=low

  * No-change rebuild to get rid of python2.4-simpleparse-mxtexttools
    dependency, Python 2.4 is no longer supported (Closes: #562414).
  * Build for sparc, to ensure it migrates to testing.

 -- Debian Python Modules Team <python-modules-team@lists.alioth.debian.org>  Sun, 03 Jan 2010 14:29:02 +0100

simpleparse (2.1.0a1-4) unstable; urgency=low

  * debian/runtests.py
    - replace 'dist-package' with 'site-package', in preparation for the
      upcoming Python 2.6 transition; thanks to Jakub Wilk for the report and
      patch; Closes: #557413
  * debian/control
    - added python-egenix-mxdatetime to b-d, to build all tests at build-time;
      thanks to Jakub Wilk for the report; Closes: #557414
    - bump Standards-Version to 3.8.3 (no changes needed)
  * debian/README.source
    - added since it's required by Policy 3.8.0

 -- Debian Python Modules Team <python-modules-team@lists.alioth.debian.org>  Wed, 02 Dec 2009 23:05:07 +0100

simpleparse (2.1.0a1-3) unstable; urgency=low

  [ Vincent Bernat ]
  * Rework debian/rules to fix FTBFS with experimental version of
    python-support and to not rely on python-support internals.
    Closes: #517309.
  * Bump Standards-Version to 3.8.1.

  [ Sandro Tosi ]
  * debian/control
    - switch Vcs-Browser field to viewsvn

 -- Vincent Bernat <bernat@debian.org>  Sat, 21 Mar 2009 13:51:38 +0100

simpleparse (2.1.0a1-2) unstable; urgency=low

  * Do not forbid to build against furture Python 2.6 and add a patch to
    remove the use of "with" as variable since it will become a reserved
    keyword.
  * Fix a bashism in debian/rules, thanks to patch from Chris Lamb
    (Closes: #484437).
  * Update Standards-Version to 3.8.0
  * Do not use XS-Python-Version since this is now deprecated by python-support
  * Test only the libraries built for the current version of Python
    (Closes: #485055), thanks to Daniel Schepler for spotting this one.

 -- Vincent Bernat <bernat@debian.org>  Mon, 16 Jun 2008 19:27:39 +0200

simpleparse (2.1.0a1-1) unstable; urgency=low

  * New upstream release (Closes: #357537).
  * Maintainership set to Debian Python Modules Team
  * Bump Standards-Version to 3.7.3
  * Build-Depends on python-setuptools
  * Add Vcs-* fields to debian/control
  * Ship three packages: one with mxtexttools, another with
    architecture-independant files and the last one with documentation and
    examples.
  * Add some cleaning rules to be able to build the package twice in a row
  * Add debian/watch
  * Run unit tests on build
  * Ship with parts of mxTextTools and therefore does not need it any more
    (Closes: #426429).
  * Acknowledge NMU (Closes: #373381).

 -- Vincent Bernat <bernat@luffy.cx>  Wed, 29 Aug 2007 11:30:36 +0200

simpleparse (2.0.0-3.1) unstable; urgency=low

  * Non-maintainer upload.
  * Update simpleparse to the new python policy (Closes: #373381).
  * Fix B-D-I/B-D problems (see policy 7.6).

 -- Pierre Habouzit <madcoder@debian.org>  Thu, 29 Jun 2006 12:17:04 +0200

simpleparse (2.0.0-3) unstable; urgency=low

  * Initial release for uploading to Debian (Closes: #271781)

 -- Seo Sanghyeon <tinuviel@sparcs.kaist.ac.kr>  Mon,  7 Mar 2005 19:33:25 +0900

simpleparse (2.0.0-2) unstable; urgency=low

  * Depend on python-dev to fix build failure
  * Correct Bulid-Depends to Build-Depends-Indep
  * Install documentation and example properly

 -- Seo Sanghyeon <tinuviel@sparcs.kaist.ac.kr>  Mon,  7 Mar 2005 13:12:56 +0900

simpleparse (2.0.0-1) unstable; urgency=low

  * Initial packaging.

 -- Seo Sanghyeon <tinuviel@sparcs.kaist.ac.kr>  Thu, 29 Jul 2004 17:28:02 +0900
