import sys
from os.path import join
from distutils.sysconfig import get_python_lib
from site import addsitedir

root = sys.argv.pop(1)
site_packages = get_python_lib().replace('dist-packages', 'site-packages')

_path = sys.path[:]
sys.path[:] = []
addsitedir(join(root, site_packages[1:]))
sys.path.extend(_path)
sys.path.append("tests")

execfile("tests/test.py")
